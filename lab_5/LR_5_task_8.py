import numpy as np
import neurolab as nl
import pylab as pl
import numpy.random as rand

skv = 0.04
center = np.array([[0.2, 0.3], [0.4, 0.4], [0.7, 0.3], [0.1, 0.5]])
rand_norm = skv * rand.randn(100, 4, 2)
inp = np.array([center + r for r in rand_norm])
inp = inp.reshape((100 * 4, 2))
rand.shuffle(inp)

net = nl.net.newc(minmax=[[0.0, 1.0], [0.0, 1.0]], cn=4)

error = net.train(inp, epochs=200)

pl.title('Classification Problem')

pl.subplot(211)
pl.plot(error)
pl.xlabel('Epoch number')
pl.ylabel('Error (default MAE)')

w = net.layers[0].np['w']

pl.subplot(212)
pl.plot(inp[:, 0], inp[:, 1], '.',
        center[:, 0], center[:, 1], 'yv',
        w[:, 0], w[:, 1], 'p'
        )

pl.legend(['Train samples', 'Real centers', 'Train centers'])
pl.show()
