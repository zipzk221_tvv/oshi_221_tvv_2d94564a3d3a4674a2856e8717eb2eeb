import numpy as np
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import PolynomialFeatures

m = 100
X = np.linspace(-3, 3, m)
y = 3 + np.sin(X) + np.random.uniform(-0.5, 0.5, m)

lin_reg = LinearRegression()
lin_reg.fit(X, y)

X_new = np.linspace(-3, 3, m).reshape(m, 1)
y_predict_lin = lin_reg.predict(X_new)


degree = 2
polyreg = make_pipeline(PolynomialFeatures(degree), LinearRegression())
polyreg.fit(X, y)

y_predict_poly = polyreg.predict(X_new)

plt.scatter(X, y, color='blue', label='Дані')
plt.plot(X_new, y_predict_poly, color='green', label=f'Прогноз (поліном {degree} ступеня)')
plt.xlabel('X')
plt.ylabel('y')
plt.legend()
plt.show()
